#%% Change working directory from the workspace root to the ipynb file location. Turn this addition off with the DataScience.changeDirOnImportExport setting
import os
try:
	os.chdir(os.path.join(os.getcwd(), 'python'))
	print(os.getcwd())
except:
	pass

#%%
# Common imports
import numpy as np
import os
import pandas as pd

# Set random seed
np.random.seed(42)

# For plotting
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rc('axes', labelsize=14)
mpl.rc('xtick', labelsize=12)
mpl.rc('ytick', labelsize=12)

# To ignore useless warnings
import warnings
warnings.filterwarnings(action="ignore", message="^internal gelsd")


#%%
TITANIC_PATH = os.path.join("..","Data")


#%%
def load_titanic_data(filename, titanic_path=TITANIC_PATH):
    csv_path = os.path.join(titanic_path,filename)
    return pd.read_csv(csv_path, index_col=0, header=0)


#%%
train_data = load_titanic_data("train.csv")
test_data = load_titanic_data("test.csv")
train_data.head()

#%% [markdown]
# The attributes have the following meaning:
# * **Survived**: that's the target, 0 means the passenger did not survive, while 1 means he/she survived.
# * **Pclass**: passenger class.
# * **Name**, **Sex**, **Age**: self-explanatory
# * **SibSp**: how many siblings & spouses of the passenger aboard the Titanic.
# * **Parch**: how many children & parents of the passenger aboard the Titanic.
# * **Ticket**: ticket id
# * **Fare**: price paid (in pounds)
# * **Cabin**: passenger's cabin number
# * **Embarked**: where the passenger embarked the Titanic

#%%
print("Name categories:",len(train_data["Name"].value_counts()))
print("Sex categories:",len(train_data["Sex"].value_counts()))
print("Ticket categories:",len(train_data["Ticket"].value_counts()))
print("Cabin categories:",len(train_data["Cabin"].value_counts()))
print("Embarked categories:",len(train_data["Embarked"].value_counts()))


#%%
train_data["Sex"].value_counts()


#%%
train_data["Embarked"].value_counts()


#%%
get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
train_data.hist(bins=50, figsize=(20,15))
plt.show()

#%% [markdown]
# # Preparation pipelines
#%% [markdown]
# Numerical pipeline

#%%
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer

num_pipeline = Pipeline([
    ('imputer', SimpleImputer(strategy='median')) # fill NAs with median value
])

#%% [markdown]
# Categorical pipeline

#%%
# Usarem un "na filler" semblant a SimpleImputer pero
# que retorni la categoria mes comu
# (stackoverflow.com/questions/25239958)

from sklearn.base import BaseEstimator, TransformerMixin

class MostFrequentImputer(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        self.most_frequent_ = pd.Series([X[c].value_counts().index[0] for c in X],
                                        index=X.columns)
        return self
    def transform(self, X, y=None):
        return X.fillna(self.most_frequent_)


#%%
from sklearn.preprocessing import OneHotEncoder

cat_pipeline = Pipeline([
    ('imputer', MostFrequentImputer()),
    ('cat_encoder', OneHotEncoder(sparse=False))
])

#%% [markdown]
# Full pipeline (apply numerical pipeline to numerical variables and so on)

#%%
from sklearn.compose import ColumnTransformer

preprocess_pipeline = ColumnTransformer([
    ('num', num_pipeline, ["Age", "SibSp", "Parch", "Fare"]),
    ('cat', cat_pipeline, ["Pclass", "Sex", "Embarked"])
])

#%% [markdown]
# #  Prepare datasets

#%%
X_train = preprocess_pipeline.fit_transform(train_data)
X_train


#%%
y_train = train_data["Survived"]


#%%
X_test = preprocess_pipeline.fit_transform(test_data)

#%% [markdown]
# # Model testing
#%% [markdown]
# ## Support Vector Classifier

#%%
from sklearn.svm import SVC

svm_clf = SVC(gamma='auto')
svm_clf.fit(X_train, y_train)


#%%
from sklearn.model_selection import cross_val_score

svm_scores = cross_val_score(svm_clf, X_train, y_train, cv=10)
svm_scores.mean()

#%% [markdown]
# ## Random Forest Classifier

#%%
from sklearn.ensemble import RandomForestClassifier

forest_clf = RandomForestClassifier(n_estimators=100, random_state=42)
forest_clf.fit(X_train, y_train)


#%%
forest_scores = cross_val_score(forest_clf, X_train, y_train, cv=10)
forest_scores.mean()

#%% [markdown]
# ## Visualize cross validation scores

#%%
plt.figure(figsize=(8, 4))
plt.plot([1]*10, svm_scores, ".")
plt.plot([2]*10, forest_scores, ".")
plt.boxplot([svm_scores, forest_scores], labels=("SVM","Random Forest"))
plt.ylabel("Accuracy", fontsize=14)
plt.show()

#%% [markdown]
# ## Search for better Random Forest hyperparameters

#%%
from sklearn.model_selection import RandomizedSearchCV
from scipy.stats import randint

forest_clf = RandomForestClassifier(random_state=42)

param_dist = {
    'n_estimators': randint(low=1, high=400),
    'max_features': randint(low=1, high=12)
}

rnd_search = RandomizedSearchCV(forest_clf, param_dist, cv=10, scoring='neg_mean_squared_error',
                           n_iter=50, random_state=42, verbose=2, n_jobs=2)
rnd_search.fit(X_train, y_train)


#%%
rnd_search.best_params_

#%% [markdown]
# # Feature engineering

#%%
class FeatureAdder(BaseEstimator, TransformerMixin):
    def __init__(self, feature1, feature2):
        self.feature1_ = feature1
        self.feature2_ = feature2
    def fit(self, X, y=None):
        return self
    def transform(self, X, y=None):
        return X[self.feature1_] + X[self.feature2_]


#%%
feature_pipeline = Pipeline([
    ('relatives', FeatureAdder("SibSp", "Parch"))
])


#%%
feature_pipeline.fit_transform(train_data)


#%%



