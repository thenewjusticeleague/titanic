# Anàlisi de dades #

Supervivents per gènere:

![Survived.png](https://bitbucket.org/repo/KEgjBq/images/4235438563-Survived.png)

Supervivents per classe:

![Class.png](https://bitbucket.org/repo/KEgjBq/images/1940903402-Class.png)

Supervivents per edat:

![Age.png](https://bitbucket.org/repo/KEgjBq/images/2696752945-Age.png)

El mateix normalitzat:

![Norm.png](https://bitbucket.org/repo/KEgjBq/images/1594728124-Norm.png)

Distribució família horizontal:

![FamiliaHorizontal.png](https://bitbucket.org/repo/KEgjBq/images/2271022263-FamiliaHorizontal.png)

Distribució família vertical:

![FamiliaVertical.png](https://bitbucket.org/repo/KEgjBq/images/1072977194-FamiliaVertical.png)

Distribució normalitzada del preu del ticket:

![Distribuci_preu.png](https://bitbucket.org/repo/KEgjBq/images/3026961546-Distribuci_preu.png)

PCA:

![Rplot.png](https://bitbucket.org/repo/KEgjBq/images/3456049348-Rplot.png)

Tot amb tot:

![Rplot.png](https://bitbucket.org/repo/KEgjBq/images/77383063-Rplot.png)